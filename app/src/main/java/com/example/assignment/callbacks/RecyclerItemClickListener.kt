package com.example.assignment.callbacks

import android.view.View

interface RecyclerItemClickListener<T> {
    fun onItemClick(position: Int, item: T, v: View?)
}


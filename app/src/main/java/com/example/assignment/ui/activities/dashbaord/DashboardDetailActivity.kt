package com.example.assignment.ui.activities.dashbaord

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.assignment.R
import com.example.assignment.base.BaseActivity
import com.example.assignment.common.ImageUtil.ImageLoader
import com.example.assignment.constant.AppConstants
import com.example.assignment.constant.AppConstants.IMAGE_URL
import com.example.assignment.databinding.ActivityDashBoardDetailBinding
import com.example.assignment.databinding.ActivityMainBinding
import java.util.ResourceBundle.getBundle

class DashboardDetailActivity : BaseActivity() {

    private var mBinding: ActivityDashBoardDetailBinding? = null
    private var largeImage: String = ""

    fun startActivity(context: Context,mImageUrl: String) {
        val intent = Intent(context, DashboardDetailActivity::class.java)
        intent.putExtra(IMAGE_URL, mImageUrl)
        context.startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(mThis, R.layout.activity_dash_board_detail)
        getBundle()
        // initialize view
        largeImage?.let { init(it) }
    }

    private fun getBundle() {
        largeImage = intent.getStringExtra(IMAGE_URL).toString()
        init(largeImage);
    }

    private fun init(imageUrl: String) {
        val loader: Int = R.drawable.ic_loader
        // ImageLoader class instance
        val imgLoader = ImageLoader(mThis)
        imgLoader.DisplayImage(imageUrl, loader, mBinding!!.largeImageview)
    }

}
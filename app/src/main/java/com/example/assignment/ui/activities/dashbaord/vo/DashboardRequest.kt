/**
 * created by Appventurez Mobitech on 13/04/21
 */
class DashboardRequest {
    var emailOrMobile: String? = null
    var password: String? = null
        get() = field        // getter
        set(value) {         // setter
            field = value
        }
}

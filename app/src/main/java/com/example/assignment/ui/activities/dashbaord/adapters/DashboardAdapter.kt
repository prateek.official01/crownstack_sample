package com.example.assignment.ui.activities.dashbaord.adapters

import JsonItunes_Base
import Results
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.assignment.R
import com.example.assignment.callbacks.RecyclerItemClickListener
import com.example.assignment.common.ImageUtil.ImageLoader
import com.example.assignment.databinding.DashboardItemBinding
import com.example.assignment.ui.activities.dashbaord.DashbaordActivity


class DashboardAdapter(
        private val mContext: DashbaordActivity,
        private val mListData: ArrayList<Results>,
        private val mClickListener: RecyclerItemClickListener<Results>
) : RecyclerView.Adapter<DashboardAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                LayoutInflater.from(mContext).inflate(
                        R.layout.dashboard_item, parent, false
                )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val eachListData = mListData[position]

        val loader: Int = R.drawable.ic_loader
        holder.mBinding?.tvTrackName?.text = eachListData.trackName
        holder.mBinding?.tvcollectionName?.text = eachListData.collectionName
        holder.mBinding?.tvArtistName?.text = eachListData.artistName
        holder.mBinding?.tvTrackPrice?.text = eachListData.trackPrice.toString()

        holder.mBinding?.mainView!!.setOnClickListener {
            if (null != mClickListener)
                mClickListener.onItemClick(position, mListData[position], holder.mBinding.root)
        }

        // ImageLoader class instance
        val imgLoader = ImageLoader(mContext)
        imgLoader.DisplayImage(eachListData.artworkUrl60, loader, holder.mBinding!!.imageUser)
    }


    override fun getItemCount(): Int {
        return mListData.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val mBinding: DashboardItemBinding? = DataBindingUtil.bind(itemView)
    }
}


import DashboardResponse
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.assignment.serivces.api.ApiService
import com.example.assignment.serivces.api.DependencyUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class DashboardRepository {
    private val apiService: ApiService
    fun dashboard(dashboardRequest: DashboardRequest): LiveData<JsonItunes_Base> {
        val data: MutableLiveData<JsonItunes_Base> =
            MutableLiveData<JsonItunes_Base>()
        apiService.dashboard().enqueue(object : Callback<JsonItunes_Base> {
            override fun onResponse(
                call: Call<JsonItunes_Base>,
                response: Response<JsonItunes_Base>
            ) {
                data.setValue(response.body())
            }

            override fun onFailure(
                call: Call<JsonItunes_Base>,
                t: Throwable
            ) {
//                data.setValue(null)
            }
        })
        return data
    }

    init {
        apiService = DependencyUtil.appService!!
    }
}
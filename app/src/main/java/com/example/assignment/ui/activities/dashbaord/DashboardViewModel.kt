package com.example.assignment.ui.activities.dashbaord

import DashboardRepository
import DashboardRequest
import DashboardResponse
import JsonItunes_Base
import android.app.Application
import android.os.Build
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.example.assignment.base.BaseViewModel
import com.example.assignment.serivces.api.AbsentLiveData


class DashboardViewModel(application: Application) : BaseViewModel(application) {
    private val dashboardRepository: DashboardRepository?
    private val dashboardRequest: MutableLiveData<DashboardRequest> = MutableLiveData()
    private val mDashboardResponse: LiveData<JsonItunes_Base>

    init {
        dashboardRepository = DashboardRepository()
        mDashboardResponse = Transformations.switchMap(dashboardRequest, {
            when {
                it == null  -> AbsentLiveData.create()
                else -> dashboardRepository.dashboard(it)
            }
        })
    }

    fun setDashboardRequest(request: DashboardRequest) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (dashboardRequest.value == request) {
                return
            }
        }
        dashboardRequest.setValue(request)
    }

    fun getDashboardResponse(): LiveData<JsonItunes_Base> {
        return mDashboardResponse
    }


}


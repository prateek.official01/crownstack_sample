package com.example.assignment.ui.activities

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.example.assignment.R
import com.example.assignment.constant.AppConstants
import com.example.assignment.ui.activities.dashbaord.DashbaordActivity

class SplashActivity : AppCompatActivity() {

    private val runnable = {
        DashbaordActivity().startActivity(this@SplashActivity)
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        moveToNext()
    }

    private fun moveToNext() {
        val handler = Handler()
        handler.postDelayed(runnable, AppConstants.SPLASH_DURATION.toLong())
    }
}

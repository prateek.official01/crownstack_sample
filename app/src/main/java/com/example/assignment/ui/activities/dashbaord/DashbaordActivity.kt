package com.example.assignment.ui.activities.dashbaord

import DashboardRequest
import DashboardResponse
import JsonItunes_Base
import Results
import android.Manifest
import android.Manifest.permission
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assignment.R
import com.example.assignment.base.BaseActivity
import com.example.assignment.callbacks.RecyclerItemClickListener
import com.example.assignment.common.DialogUtil
import com.example.assignment.common.Utilities
import com.example.assignment.constant.AppConstants.IMAGE_URL
import com.example.assignment.databinding.ActivityMainBinding
import com.example.assignment.serivces.api.ApiConstant.WRITE_REQUEST_CODE
import com.example.assignment.ui.activities.dashbaord.adapters.DashboardAdapter

class DashbaordActivity : BaseActivity(), RecyclerItemClickListener<Results> {

    private var mBinding: ActivityMainBinding? = null
    private var mDashboardVM: DashboardViewModel? = null

    fun startActivity(context: Context) {
        val intent = Intent(context, DashbaordActivity::class.java)
        context.startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(mThis, R.layout.activity_main)
        // initialize view
        init()
    }

    // initialized all view model and data here
    private fun init() { // register event bus
        // initialized login view model here for reference purpose
        mDashboardVM = ViewModelProviders.of(this)[DashboardViewModel::class.java]

        val mLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        mBinding!!.rvDashboard.setLayoutManager(mLayoutManager)

        // Check the Storage write functionality
        setupPermissions()
    }

    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(this,
                permission.WRITE_EXTERNAL_STORAGE)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            makeRequest()
        }else{
            bindRecyclerViewData()
        }
    }

    private fun bindRecyclerViewData() {
        if (Utilities.getNetworkState(this))
            callItunesLocalData();
        else
            Toast.makeText(this, R.string.no_internet_error_msg, Toast.LENGTH_SHORT).show()
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(this,
                arrayOf(permission.WRITE_EXTERNAL_STORAGE), WRITE_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            WRITE_REQUEST_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(mThis, permission.WRITE_EXTERNAL_STORAGE)) {
                        // we can request the permission.
                        makeRequest()
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {
                        DialogUtil.showPermissionSnackBar(mBinding!!.main,getString(R.string.settings_permission_meg))
                        // MY_PERMISSIONS_REQUEST_WRITE_STORAGE is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                } else {
                    bindRecyclerViewData()
                }
            }
        }
    }

    //method to call the login api
    private fun callItunesLocalData() {
        showHideProgressDialog(true)
        var response = Utilities.getITunesResult(this)
        response?.let { handleLocalITunesResponse(it) }
    }

    private fun handleLocalITunesResponse(it: JsonItunes_Base) {
        showHideProgressDialog(false)
        if (it != null) {
            setAdapter(it)
        } else
            DialogUtil.showSnackBar(mBinding!!.root, R.string.something_went_wrong)
    }

    //method to call the if need to call itunes api for reference purpose
    private fun callItunesApi() {
        showHideProgressDialog(true)
        handleiTunesDataRequest()?.let { mDashboardVM!!.setDashboardRequest(it) }
        if (!mDashboardVM!!.getDashboardResponse().hasActiveObservers()) {
            mDashboardVM!!.getDashboardResponse().observe(
                    this,
                    Observer { response: JsonItunes_Base -> handleApiItunesResponse(response) })
        }
    }

    // get iTunes request params for reference purpose
    private fun handleiTunesDataRequest(): DashboardRequest? {
        val dashboardRequest = DashboardRequest()
        /* dashboardRequest.emailOrMobile = mBinding!!.emailEdtxt.text.toString().trim()
         dashboardRequest.password = mBinding!!.passwordEdtxt.text.toString().trim()*/
        return dashboardRequest
    }


    // handle iTunesData response
    private fun handleApiItunesResponse(response: JsonItunes_Base) {
        showHideProgressDialog(false)
        if (response != null) {
            setAdapter(response)
        } else
            DialogUtil.showSnackBar(mBinding!!.root, R.string.something_went_wrong)
    }


    private fun setAdapter(response: JsonItunes_Base) {
        val dashboardAdapter = DashboardAdapter(this, response.results as ArrayList<Results>,this)
        mBinding!!.rvDashboard.setAdapter(dashboardAdapter)
    }

    override fun onItemClick(position: Int, item: Results, v: View?) {
        DashboardDetailActivity().startActivity(this,item.artworkUrl100)
    }
}
package com.example.assignment.base

import android.app.Application
import com.example.assignment.common.Utilities
import com.example.assignment.common.network.NetworkHandler


class AppController : Application() {

    private val activity: BaseActivity? = null

    override fun onCreate() {
        super.onCreate()
        mInstance = this
        NetworkHandler.isConnect = Utilities.getNetworkState(this)
    }


    fun requireActivity(): BaseActivity? {
        return activity
    }

    companion object {
        @get:Synchronized
        private val TAG = "ClipsCutsApplication"
        private var live = false
        var mInstance: AppController? = null


        fun islifeMethod(): Boolean {
            return live
        }

        fun setIslifeMethod(islifeMethod: Boolean) {
            AppController.live = islifeMethod
        }

    }


}

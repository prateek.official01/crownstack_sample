package com.example.assignment.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel


/**
 * Application context aware [ViewModel].
 *
 *
 * Subclasses must have a constructor which accepts [Application] as the only parameter.
 *
 *
 */
open class BaseViewModel(application: Application) : AndroidViewModel(application)

package com.example.assignment.base

import android.Manifest
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.util.Size
import android.view.View
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.assignment.common.DialogUtil
import com.example.assignment.common.Utilities


open class BaseActivity : AppCompatActivity(), BaseListener, View.OnClickListener {
    var mActionBar: ActionBar? = null
    var mToolBar: Toolbar? = null
    var mToolbarTitle: TextView? = null
//    var navigation: NavigationUtils? = null
    var mPrintPreview: ImageView? = null
    var mProgressBar: ProgressBar? = null
    var backpress: Int = 0
     lateinit var mThis: BaseActivity
    private val mDialog: ProgressDialog? = null
    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Initialize Places.
        mThis = this
    }




    override fun showHideProgressDialog(isShow: Boolean) {
        if (progressDialog != null) {
            if (progressDialog!!.isShowing) {
                if (!isShow) {
                    progressDialog!!.dismiss()
                    progressDialog = null
                } else
                    return
            } else {
                if (isShow)
                    progressDialog!!.show()
            }
        } else {
            progressDialog = DialogUtil.getProgressDialog(mThis)
            showHideProgressDialog(isShow)

        }
    }

    override  fun showHideProgressBar(iShow: Boolean) {
        if (mProgressBar != null) {
            mProgressBar!!.visibility = View.GONE
            if (iShow)
                mProgressBar!!.visibility = View.VISIBLE
            else {
                mProgressBar!!.visibility = View.GONE
                mProgressBar = null
            }
        } else {
//            mProgressBar = DialogUtil.getProgressBarInstance(mThis, R.id.circular_progress_bar)
            if (mProgressBar == null) return
            showHideProgressBar(iShow)
        }
    }

    override fun onClick(view: View) {
        Utilities.hideKeyboard(mThis)
        when (view.id) {

        }
    }


    override fun onBackPressed() {
        finish()
    }


    /**
     * This method invokes when user press back button of header or device as well.
     */
    fun onHeaderBackPress() {
        Utilities.hideKeyboard(this)
        super.onBackPressed()
    }

    //    @Override
    //    protected void attachBaseContext(Context newBase) {
    //        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    //    }



    companion object {

        val ALL_PERMISSIONS = 10001
        val CAMERA_PERMISSION = 20002
        val LOCATION_PERMISSION = 30003

        fun checkPermissions(activity: Activity): Boolean {

            Log.d("PERMISSION_CHECK", "LOCATION: ")
            if (ContextCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        activity,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                ) {

                    ActivityCompat.requestPermissions(
                        activity,
                        arrayOf(
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.CAMERA
                        ),
                        ALL_PERMISSIONS
                    )
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                    Log.d("PERMISSION_CHECK", "SHOW RATIONALE: ")
                } else {
                    // No explanation needed; request the permission
                    ActivityCompat.requestPermissions(
                        activity,
                        arrayOf(
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.CAMERA
                        ),
                        ALL_PERMISSIONS
                    )
                    Log.d("PERMISSION_CHECK", "NO RATIONALE: ")

                }
                // Permission is not granted
                return false
            } else {
                return true
            }
        }

        //    @Override
        //    protected void attachBaseContext(Context newBase) {
        //        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
        //    }


        fun checkCameraPermission(activity: Activity): Boolean {

            Log.d("PERMISSION_CHECK", "LOCATION: ")
            if (ContextCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        activity,
                        Manifest.permission.CAMERA
                    )
                ) {

                    ActivityCompat.requestPermissions(
                        activity,
                        arrayOf(Manifest.permission.CAMERA),
                        CAMERA_PERMISSION
                    )
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                    Log.d("PERMISSION_CHECK", "SHOW RATIONALE: ")
                } else {
                    // No explanation needed; request the permission
                    ActivityCompat.requestPermissions(
                        activity,
                        arrayOf(Manifest.permission.CAMERA),
                        CAMERA_PERMISSION
                    )
                    Log.d("PERMISSION_CHECK", "NO RATIONALE: ")

                }
                // Permission is not granted
                return false
            } else {
                return true
            }
        }

        fun checkLocationPermission(activity: Activity): Boolean {

            Log.d("PERMISSION_CHECK", "LOCATION: ")
            if (ContextCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        activity,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                ) {

                    ActivityCompat.requestPermissions(
                        activity,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        LOCATION_PERMISSION
                    )
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                    Log.d("PERMISSION_CHECK", "SHOW RATIONALE: ")
                } else {
                    // No explanation needed; request the permission
                    ActivityCompat.requestPermissions(
                        activity,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        LOCATION_PERMISSION
                    )
                    Log.d("PERMISSION_CHECK", "NO RATIONALE: ")

                }
                // Permission is not granted
                return false
            } else {
                return true
            }
        }
    }

}

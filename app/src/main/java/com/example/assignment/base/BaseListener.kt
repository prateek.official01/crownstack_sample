package com.example.assignment.base

interface BaseListener {

    /**
     * Method to show a progress dialog on some background task
     */
    fun showHideProgressDialog(iShow: Boolean)

    /**
     * Method to show a progress bar on some background task
     */
    fun showHideProgressBar(iShow: Boolean)


}

package com.example.assignment.serivces.api

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData


object ApiUtil {
    fun <T> successCall(data: T): LiveData<T> {
        return createCall(data)
    }

    fun <T> createCall(response: T): LiveData<T> {
        val data = MutableLiveData<T>()
        data.value = response
        return data
    }
}

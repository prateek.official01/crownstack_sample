package com.example.assignment.serivces.api

import androidx.appcompat.app.AlertDialog
import com.example.assignment.BuildConfig
import com.example.assignment.common.LiveDataCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class AppRetrofit {

    val apiService: ApiService
    private val isLogoutDialogShowing: Boolean = false
    private val authRequired = true

    private constructor() {
        apiService = provideService(BuildConfig.BASE_URL)
    }

    constructor(BaseUrl: String) {
        apiService = provideService(BaseUrl)
    }

    private fun provideService(BaseUrl: String): ApiService {

        // To show the Api Request & Params
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val httpClient = OkHttpClient.Builder()
        httpClient.connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
        httpClient.addInterceptor(logging).addInterceptor { chain ->
            val requestBuilder = chain.request().newBuilder()
                .addHeader(ApiConstant.KEY_CONTENT_TYPE, ApiConstant.CONTENT_TYPE)
            val request = requestBuilder
                .addHeader(ApiConstant.LOCALE, "en")
                .addHeader(ApiConstant.ACCESS_TOKEN, "")//SessionManager.get().getAccessToken()
                .addHeader(ApiConstant.KEY_CONTENT_TYPE, ApiConstant.CONTENT_TYPE)
                .build()
            val response = chain.proceed(request)
            //                Lg.e("AppRetrofit", SessionManager.get().getAccessToken());

            if (response.isSuccessful && response.code() == 202) {
                if (mAlertDialog == null) {
                    //                        logout("You are already logged in on another device or your session expired");
                } else if (!mAlertDialog.isShowing) {
                    //                        logout("You are already logged in on another device or your session expired");
                }
            }
            response
        }
        return Retrofit.Builder()
            .baseUrl(BaseUrl).addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .client(httpClient.build()).build().create(ApiService::class.java!!)
    }

    companion object {

        private var instance: AppRetrofit? = null
        private val mAlertDialog: AlertDialog? = null


        private fun initInstance() {
            if (instance == null) {
                // Create the instance
                instance = AppRetrofit()
            }
        }

        fun getInstance(): AppRetrofit? {
            // Return the instance
            initInstance()
            return instance
        }
    }
}

package com.example.assignment.serivces.api

import androidx.appcompat.app.AlertDialog
import com.example.assignment.BuildConfig
import com.example.assignment.common.LiveDataCallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit


class AppRetrofitMultipart private constructor() {
    val appService: ApiService
    private val isLogoutDialogShowing: Boolean = false
    private val mAlertDialog: AlertDialog? = null
    private val authRequired = true

    init {
        appService = providService(BuildConfig.BASE_URL)
    }

    private fun providService(BaseUrl: String): ApiService {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val httpClient = OkHttpClient.Builder()
        httpClient.connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
        httpClient.addInterceptor(logging).addInterceptor { chain ->
            val requestBuilder = chain.request().newBuilder()
                .addHeader(ApiConstant.KEY_CONTENT_TYPE, ApiConstant.CONTENT_TYPE_MULTIPART)
            val request = requestBuilder
                .addHeader(ApiConstant.ACCESS_TOKEN,"")// SessionManager.get().getAccessToken()
                .addHeader(ApiConstant.KEY_CONTENT_TYPE, ApiConstant.CONTENT_TYPE_MULTIPART)
                .build()
            chain.proceed(request)
        }
        return Retrofit.Builder()
            .baseUrl(BaseUrl).addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .client(httpClient.build()).build().create(ApiService::class.java!!)
    }

    companion object {

        private var instance: AppRetrofitMultipart? = null

        private fun initInstance() {
            if (instance == null) {
                // Create the instance
                instance = AppRetrofitMultipart()
            }
        }

        fun getInstance(): AppRetrofitMultipart? {
            // Return the instance
            initInstance()
            return instance
        }
    }
}

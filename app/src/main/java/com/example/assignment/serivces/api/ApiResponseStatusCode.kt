package com.example.assignment.serivces.api

/**
 * Constant class to hold network responses
 */
object ApiResponseStatusCode {

    val DATABASE_ERROR = 500
    val SUCCESS_200 = 200
    val SUCCESS_300 = 300
    val INTERNET_ERROR = 503
}

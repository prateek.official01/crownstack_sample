package com.example.assignment.serivces.api

import android.util.Log
import retrofit2.Response
import java.io.IOException
import java.net.UnknownHostException


/**
 * Generic class to hold response from Apis
 *
 * @param <T> : respective response model class
</T> */
class ApiResponse<T> {

    val code: Int
    val body: T?
    val errorMessage: String?
    var TAG = "Api Response"

    val isSuccessful: Boolean
        get() = code >= ApiResponseStatusCode.SUCCESS_200 && code < ApiResponseStatusCode.SUCCESS_300

    constructor(error: Throwable) {
        if (error is UnknownHostException)
            code = ApiResponseStatusCode.INTERNET_ERROR
        else
            code = ApiResponseStatusCode.DATABASE_ERROR
        body = null
        errorMessage = error.message
    }

    constructor(response: Response<T>) {
        code = response.code()
        if (response.isSuccessful) {
            body = response.body()
            errorMessage = null
        } else {
            var message: String? = null
            if (response.errorBody() != null) {
                try {
                    message = response.errorBody()!!.string()
                } catch (ignored: IOException) {
                    Log.e(TAG, "error while parsing response")
                }

            }
            if (message == null || message.trim { it <= ' ' }.length == 0) {
                message = response.message()
            }
            errorMessage = message
            body = null
        }

    }
}

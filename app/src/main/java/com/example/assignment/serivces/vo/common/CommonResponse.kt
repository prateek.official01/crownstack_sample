package com.example.assignment.serivces.vo.common


open class CommonResponse {


    /**
     * success : true
     * message : success
     * responseCode : 200
     * time : 1577522868610
     */

    var isSuccess: Boolean = false
    var message: String? = null
        get() = if (field == null) "" else field
    var responseCode: Int = 0
    var time: Long = 0
}


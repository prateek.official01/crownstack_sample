package com.example.assignment.serivces.api

object DependencyUtil {

     var sApiServiceInstance: ApiService? = null


    val appService: ApiService?
        get() {
            if (sApiServiceInstance == null) sApiServiceInstance = AppRetrofit.getInstance()
                ?.apiService
            return sApiServiceInstance
        }

    val appServices: ApiService?
        get() {
            if (sApiServiceInstance == null) sApiServiceInstance = AppRetrofit.getInstance()
                ?.apiService
            return sApiServiceInstance
        }


    val appExecuter: AppExecutors
        get() = AppExecutors()
}
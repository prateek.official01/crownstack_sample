package com.example.assignment.serivces.api

import DashboardResponse
import JsonItunes_Base
import retrofit2.Call
import retrofit2.http.*


/**
 * All API services, with their Url, Response type, Request type and Request method(eg. GET, POST)
 */
interface ApiService {

    //method for iTunesData api
    @GET()
    fun dashboard():Call<JsonItunes_Base>

}

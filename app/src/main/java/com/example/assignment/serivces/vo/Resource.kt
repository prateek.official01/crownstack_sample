package com.example.assignment.serivces.vo


/**
 * A generic class that holds a value with its loading status.
 *
 * @param <T>
</T> */
class Resource<T>(
    val status: Status/*, @Nullable T result*/,
    /*@Nullable
    public final T result;*/

    val data: T?,
    val message: String?,
    val code: Int,
    val api_status: Boolean,
    val user_exists: Boolean
)//this.result=result;
{

    lateinit var token: String

    override fun equals(o: Any?): Boolean {
        if (this === o) {
            return true
        }
        if (o == null || javaClass != o.javaClass) {
            return false
        }

        val resource = o as Resource<*>?

        if (status !== resource!!.status) {
            return false
        }
        if (if (message != null) message != resource!!.message else resource!!.message != null) {
            return false
        }
        return if (data != null) data == resource.data else resource.data == null
    }

    override fun hashCode(): Int {
        var result = status.hashCode()
        result = 31 * result + (message?.hashCode() ?: 0)
        result = 31 * result + (data?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "Resource{" +
                "status=" + status +
                ", message='" + message + '\''.toString() +
                ", data=" + data +
                '}'.toString()
    }

    companion object {

        fun <T> success(
            data: T?,
            code: Int,
            api_status: Boolean,
            user_exists: Boolean
        ): Resource<T> {
            return Resource(Status.SUCCESS, data, null, code, api_status, user_exists)
        }

        fun <T> error(
            msg: String,
            data: T?,
            code: Int,
            api_status: Boolean,
            user_exists: Boolean
        ): Resource<T> {
            return Resource(Status.ERROR, data, msg, code, api_status, user_exists)
        }


        /*public static <T> Resource<T> loading(@Nullable T result, int code) {
        return new Resource<>(LOADING, result, null, code);
    }*/

        fun <T> loading(
            data: T?,
            code: Int,
            api_status: Boolean,
            user_exists: Boolean
        ): Resource<T> {
            return Resource(Status.LOADING, data, null, code, api_status, user_exists)
        }
    }
}

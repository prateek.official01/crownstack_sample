package com.example.assignment.serivces.api


object ApiConstant {

    val KEY_CONTENT_TYPE = "Content-Type"
    val CONTENT_TYPE_MULTIPART = ""
    val CONTENT_TYPE = "application/json"
    val ACCESS_TOKEN = "accesstoken"
    val LOCALE = "locale"
    val WRITE_REQUEST_CODE=3

   const val DASHBOARD = "users"
}

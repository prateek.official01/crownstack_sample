package com.example.assignment.serivces.vo.common

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ErrorResponse {

    @SerializedName("Status")
    @Expose
    var status: Int? = null

    @SerializedName("Message")
    @Expose
    var message: String? = null
    @SerializedName("Success")
    @Expose
    var success: Boolean? = null
}

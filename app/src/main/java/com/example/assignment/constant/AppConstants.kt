package com.example.assignment.constant

object AppConstants {
    val SPLASH_DURATION = 2000
    val TOAST_DURATION = 3000
    val IMAGE_URL= "image_url"
}
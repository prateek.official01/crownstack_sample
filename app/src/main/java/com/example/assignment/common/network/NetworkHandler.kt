package com.example.assignment.common.network

import android.content.Context
import android.view.View
import com.example.assignment.common.DialogUtil
import com.example.assignment.common.Utilities

class NetworkHandler {
    companion object {

       public var isConnect: Boolean = false

        fun isConnected(): Boolean {
            return isConnect
        }


        fun isConnected(anyView: View): Boolean {
            isConnect = Utilities.getNetworkState(anyView.context)
            if (!isConnect) {
                if (anyView != null) {
                    DialogUtil.showNoNetworkSnackBar(anyView)
                    isConnect = Utilities.getNetworkState(anyView.context)
                }
            }
            return isConnect
        }
    }

}
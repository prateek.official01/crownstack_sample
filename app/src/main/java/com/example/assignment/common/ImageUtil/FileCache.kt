package com.example.assignment.common.ImageUtil

import android.app.Activity
import android.content.Context
import android.os.Environment
import com.example.assignment.ui.activities.dashbaord.DashbaordActivity
import java.io.File

class FileCache(mContext: Activity?)
{
    private var cacheDir: File? = null
    init {
        cacheDir =
            if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) File(
                Environment.getExternalStorageDirectory(), "TempImages"
            ) else mContext?.cacheDir
        if (!cacheDir!!.exists()) cacheDir!!.mkdirs()
    }

    fun FileCache(context: Context) {
        //Find the dir to save cached images
        cacheDir =
            if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) File(
                Environment.getExternalStorageDirectory(), "TempImages"
            ) else context.cacheDir
        if (!cacheDir!!.exists()) cacheDir!!.mkdirs()
    }

    fun getFile(url: String): File? {
        val filename = url.hashCode().toString()
        return File(cacheDir, filename)
    }

    fun clear() {
        val files = cacheDir!!.listFiles() ?: return
        for (f in files) f.delete()
    }
}
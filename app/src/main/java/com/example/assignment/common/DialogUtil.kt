package com.example.assignment.common

import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.graphics.Color.*
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.example.assignment.R
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.make

object DialogUtil {

        fun showNoNetworkSnackBar(anyView: View): Snackbar {
            return showSnackBar(anyView, R.string.no_internet_error_msg)
        }


        /**
         * get a blocking progress dialog.
         *
         * @param context context of current activity/fragment
         * @return create of [ProgressDialog]
         */
        fun getProgressDialog(context: Context?): ProgressDialog? {
            val progressDialog = ProgressDialog(context, R.style.ProgreTheme)
            //        progressDialog.setMessage(context.getResources().getString(R.string.please_wait));
            progressDialog.setCancelable(false)
            return progressDialog
        }


        fun showSnackBar(anyView: View, msg: Int): Snackbar {
            val res = anyView.context.resources
            return showSnackBar(anyView, msg)
        }

        fun showPermissionSnackBar(anyView: View, msg: String) {
            //Snackbar(view)
           /* val snackbar = Snackbar.make(anyView, String,
                    Snackbar.LENGTH_LONG).setAction("Action", null)
            snackbar.setActionTextColor(BLUE)
            val snackbarView = snackbar.view
            snackbarView.setBackgroundColor(LTGRAY)
            val textView =
                    snackbarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
            textView.setTextColor(BLUE)
            textView.textSize = 28f
            snackbar.show()*/


            val snackBar = Snackbar.make(
                    anyView, msg,
                    Snackbar.LENGTH_LONG
            ).setAction("Action", null)
            snackBar.setActionTextColor(Color.BLUE)
            val snackBarView = snackBar.view
            snackBarView.setBackgroundColor(Color.CYAN)
            val textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
            textView.setTextColor(Color.BLUE)
            snackBar.show()
        }

        fun Context.toast(
            context: Context = applicationContext,
            message: String,
            duration: Int = Toast.LENGTH_SHORT
        ) {
            Toast.makeText(context, message, duration).show()
        }
}
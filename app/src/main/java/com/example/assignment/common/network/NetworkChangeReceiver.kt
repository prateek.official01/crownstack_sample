package com.example.assignment.common.network

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.assignment.common.Utilities


/**
 * The type Network change receiver.
 */

/**
 * Instantiates a new Network change receiver.
 */
class NetworkChangeReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        NetworkHandler.isConnect = Utilities.getNetworkState(context)
        if (connectivityReceiverListener != null)
            connectivityReceiverListener!!.onNetworkConnectionChanged(NetworkHandler.isConnect)
    }


    /**
     * The interface Connectivity receiver listener.
     */
    interface ConnectivityReceiverListener {
        /**
         * This method is invoked bu receiver when internet connection enables or disables.
         *
         * @param isConnected network connectivity status.
         */
        fun onNetworkConnectionChanged(isConnected: Boolean)
    }

    companion object {

        /**
         * The constant connectivityReceiverListener.
         */
        var connectivityReceiverListener: ConnectivityReceiverListener? = null
    }

}

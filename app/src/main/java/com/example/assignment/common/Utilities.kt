package com.example.assignment.common

import JsonItunes_Base
import android.app.Activity
import android.content.Context
import android.content.Context.INPUT_METHOD_SERVICE
import android.net.ConnectivityManager
import android.view.inputmethod.InputMethodManager
import com.google.gson.Gson
import java.io.InputStream
import java.io.OutputStream
import java.nio.charset.StandardCharsets


class Utilities
{
    companion object {

        /**
         * Method to hide keyboard
         *
         * @param contex Context of the calling class
         */
        fun hideKeyboard(contex: Context) {
            try {
                val inputManager =
                    contex.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                inputManager.hideSoftInputFromWindow(
                    (contex as Activity).currentFocus!!.windowToken,
                    0
                )
            } catch (ignored: Exception) {
            }

        }


        /**
         * Gets network state.
         *
         * @param context the context
         * @return the network state
         */
        fun getNetworkState(context: Context): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            return activeNetwork != null && activeNetwork.isConnectedOrConnecting
        }

        fun getITunesResult(context: Context): JsonItunes_Base? {
            var json: String? = null
            var support: JsonItunes_Base? = null
            try {
                val `is` = context.assets.open("i_tunes_response")
                val size = `is`.available()
                val buffer = ByteArray(size)
                `is`.read(buffer)
                `is`.close()
                json = String(buffer, StandardCharsets.UTF_8)
                val gson = Gson()
                support = gson.fromJson(json, JsonItunes_Base::class.java)
                return support
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            return null
        }

        fun CopyStream(`is`: InputStream, os: OutputStream) {
            val buffer_size = 1024
            try {
                val bytes = ByteArray(buffer_size)
                while (true) {
                    val count: Int = `is`.read(bytes, 0, buffer_size)
                    if (count == -1) break
                    os.write(bytes, 0, count)
                }
            } catch (ex: java.lang.Exception) {
            }
        }
    }


}
